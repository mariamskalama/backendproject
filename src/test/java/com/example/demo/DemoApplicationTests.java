package com.example.demo;

import com.example.demo.exceptions.DataNotFoundException;
import com.example.demo.models.MenuItem;
import com.example.demo.models.Order;
import com.example.demo.models.User;
import com.example.demo.repositories.MenuItemRepository;
import com.example.demo.repositories.OrderRepository;
import com.example.demo.repositories.UserRepository;
import com.example.demo.services.MenuItemService;
import com.example.demo.services.UserService;
import com.example.demo.services.OrderService;
import org.aspectj.lang.annotation.After;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class DemoApplicationTests {
	@Autowired
	 UserService userService;
	@MockBean
	 UserRepository userRepository;
	@Autowired
	 MenuItemService itemService;
	@MockBean
	 MenuItemRepository itemRepository;
	@Autowired
	 OrderService orderService;
	@MockBean
	 OrderRepository orderRepository;
	User testUser;

	@AfterEach
	public void validate() {
		validateMockitoUsage();
	}

@Nested
class UserTests{


	@Test
	@DisplayName("Getting all users")
	public void test_GetAllUsers_ShouldBe2(){
		when(userRepository.findAll()).thenReturn(Stream.of
				(new User(1,"admin","test",true,"ADMIN"),
				new User(2,"user","test",true,"USER")).
				collect(Collectors.toList()));
	         	assertEquals(2,userService.getAllUsers().size());

	}

	@Test
	@DisplayName("Creating new user")
	public void test_RegisterNewUserAccountByUsingUserNameAndPassword_ShouldReturnRegisteredUser () {
			when(userRepository.save(testUser)).thenReturn(testUser);
			assertEquals(userService.addUser(testUser),testUser);
		}
	@Test
	@DisplayName("Checking save method gets called")
	public void test_AddNewUser_ShouldCallSaveMethodInRepositoryOnce () {
		User testUser = new User(1, "test", "Test", true, "USER");
		UserRepository userRepository =spy(UserRepository.class);
		UserService userService=new UserService(userRepository);
		userService.addUser(testUser);
		verify(userRepository,times(1)).save(testUser);
	}

}
	@Nested

	class OrderTests {

			@Test
			@DisplayName("Exception throwing when getting not found order")

			public void test_GetNotFoundOrder_ShouldThrowDataNotFoundException() {

				when(orderRepository.findAll()).thenReturn(Stream.of
						(new Order(1, testUser, new HashSet<MenuItem>()),
								new Order(2, testUser, new HashSet<MenuItem>())).
						collect(Collectors.toList()));
				assertThrows(DataNotFoundException.class, () -> orderService.getAllOrders(60));

			}

			@Test
			@DisplayName("Checking addorder method gets called when adding order")

			public void test_AddNewOrder_ShouldCallAddOrderOnce() {
				User testUser = new User(1, "test", "Test", true, "USER");
				Order testOrder = new Order(1, testUser, new HashSet<MenuItem>());
				OrderService orderService = mock(OrderService.class);
				orderService.addOrder(testOrder, testUser.getUserName());
				verify(orderService, times(1)).addOrder(testOrder, testUser.getUserName());
			}


			@Test
			@DisplayName("Checking save method gets called when adding order")

			public void test_AddNewOrder_ShouldCallSaveTwice () {
				User testUser = new User(1, "test", "Test", true, "USER");
				OrderRepository orderRepository=spy(OrderRepository.class);
				Order testOrder=new Order(1,testUser,new HashSet<MenuItem>());
				orderRepository.save(testOrder);
				orderRepository.save(testOrder);
				verify(orderRepository,times(2)).save(testOrder);
			}

			@Test
			@DisplayName("Checking save method gets called")

			public void test_OrderBelongsToUser_ShouldReturnTrue(){
				User testUser = new User(1, "test", "Test", true, "USER");
				Order testOrder = new Order(1, testUser, new HashSet<MenuItem>());
				Order spyOrder=spy(testOrder);
				OrderRepository orderRepository=spy(OrderRepository.class);
				UserRepository userRepository=spy(UserRepository.class);
                UserService userService=new UserService(userRepository);
				OrderService orderService=new OrderService(orderRepository,userService);
				orderService.addOrder(testOrder,testUser.getUserName());
				verify(orderRepository,times(1)).save(testOrder);
				assertSame(testUser,spyOrder.getUser());
			}

		}
			@Nested

			class ItemTests{



				@Test
				@DisplayName("Getting all menu items")
				public void test_GetAllMenuItems_ShouldReturnOneItemWithQTY1AndNullOrders(){
				when(itemRepository.findAll()).thenReturn(Stream.of
						(new MenuItem("testItem",20,1)).
						collect(Collectors.toList()));
					assertAll(
						()->assertSame(new MenuItem("testItem",20,1).getName(),itemService.getAllMenuItems().get(0).getName()),
						()->assertEquals(new MenuItem("testItem",20,1).getQty(),itemService.getAllMenuItems().get(0).getQty()),
						()->assertNull(itemService.getAllMenuItems().get(0).getOrder()));
			}

				@Test
				@DisplayName("placing an order including menu items")
				public void test_PlaceOrder_ShouldSaveNewOrderAndConnectItToMenuItems() {
				OrderRepository orderRepository = mock(OrderRepository.class);
				UserRepository userRepository = spy(UserRepository.class);
				UserService userService = new UserService(userRepository);
				OrderService orderService = new OrderService(orderRepository, userService);
				MenuItemRepository itemRepository =mock(MenuItemRepository.class);
				MenuItemService itemService=new MenuItemService(itemRepository,orderRepository,userService);
				MenuItemService spyService=spy(itemService);
				Set<MenuItem> menuItemSet=new HashSet<MenuItem>();
				MenuItem testItem=new MenuItem("testItem", 20,1);
				menuItemSet.add(testItem);
				User testUser = new User(1, "test", "Test", true, "USER");

				spyService.addMenuItemToCart(menuItemSet, testUser.getUserName());
				assertNotNull(testItem.getOrder());
				for(Order order: testItem.getOrder()){
				verify(orderRepository,times(1)).save(order);
				assertNotNull(order.getItems());}




				}








}}





