package com.example.demo.filters;

import com.example.demo.services.UserService;
import com.example.demo.util.JwtUtil;
import org.apache.log4j.BasicConfigurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


@Component
public class JwtRequestFilter extends OncePerRequestFilter {
@Autowired
private UserService userDetailsService;
@Autowired
private JwtUtil jwtUtil;
    private static final Logger logger = LogManager.getLogger(JwtRequestFilter.class);




    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        BasicConfigurator.configure();

        logger.info("in Authorization Filter" );
    final String authorizationHeader= httpServletRequest.getHeader("Authorization");

    String username=null;
    String jwt=null;
    if(authorizationHeader!=null && authorizationHeader.startsWith("Bearer ")){
        jwt=authorizationHeader.substring(7);
        username=jwtUtil.extractUsername(jwt);
    }
    if(username!=null && SecurityContextHolder.getContext().getAuthentication()==null){
        UserDetails userDetails=this.userDetailsService.loadUserByUsername(username);
        if(jwtUtil.validateToken(jwt,userDetails)){
            UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken=new UsernamePasswordAuthenticationToken(
                    userDetails,null,userDetails.getAuthorities());
            usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }

    }
    filterChain.doFilter(httpServletRequest,httpServletResponse);
        logger.info("Authorized" );

    }
}
