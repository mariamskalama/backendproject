package com.example.demo.repositories;

import com.example.demo.models.MenuItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MenuItemRepository extends CrudRepository <MenuItem,String>{

    public List<MenuItem> findByOrderId(int orderId);


}
