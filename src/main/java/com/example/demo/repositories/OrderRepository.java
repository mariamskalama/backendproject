package com.example.demo.repositories;

import com.example.demo.models.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository <Order,String>{
public List<Order> findByUserId(int userId);
public List<Order> findByUserUserName(String userName);

public List<Order> findById(int Id);


}
