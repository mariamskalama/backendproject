package com.example.demo.exceptions;

import com.example.demo.models.ErrorMessage;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {
    private static final Logger logger = LogManager.getLogger(DataNotFoundExceptionMapper.class);

    @Override
    public Response toResponse(Throwable exception) {
        BasicConfigurator.configure();

        logger.info("Throwing Generic Exception" );
        ErrorMessage errorMessage=new ErrorMessage(exception.getMessage(), 500);
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .type(MediaType.APPLICATION_JSON)
                .entity(errorMessage)
                .build();    }
}
