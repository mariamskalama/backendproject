package com.example.demo.exceptions;

import com.example.demo.filters.JwtRequestFilter;
import com.example.demo.models.ErrorMessage;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;


import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DataNotFoundExceptionMapper  implements ExceptionMapper<DataNotFoundException> {
    private static final Logger logger = LogManager.getLogger(DataNotFoundExceptionMapper.class);

    @Override
public Response toResponse(DataNotFoundException exception) {
        BasicConfigurator.configure();

        logger.info("Throwing Data Not Found Exception" );
    ErrorMessage errorMessage=new ErrorMessage(exception.getMessage(), 404);
    return Response.status(Response.Status.NOT_FOUND)
            .type(MediaType.APPLICATION_JSON)
            .entity(errorMessage)
            .build();    }}

