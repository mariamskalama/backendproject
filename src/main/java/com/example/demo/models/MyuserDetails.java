package com.example.demo.models;

import com.example.demo.models.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyuserDetails implements UserDetails {
    public static int id;
   private String username;
   private String password;
   private boolean active;
   private Set<MenuItem> menuItems=new HashSet<>();





    public MyuserDetails(User user){
        this.id=id;
        this.username=user.getUserName();
        this.password=user.getPassword();
        this.active=user.isActive();
       // this.authorities= Arrays.stream(user.getRoles().split(","))
               // .map(SimpleGrantedAuthority::new).collect(Collectors.toList());
   }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }

    public Set<MenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(HashSet<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

}
