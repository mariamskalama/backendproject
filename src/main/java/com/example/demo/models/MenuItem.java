package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Set;

@Entity

@Table(name="items")
public class MenuItem {
    @Id
    @Column(name = "name", nullable = false)
    private String Name;
    private double Price;
    private int Qty;
    @ManyToMany (mappedBy = "item" , fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Set<Order> order;
    public MenuItem(){};
    public MenuItem(String name, double price, int qty) {
        super();
        Name = name;
        Price = price;
        Qty = qty;

    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public void setOrder(Set<Order> order) {
        this.order = order;
    }
    public Set<Order> getOrder() {
        return this.order;
    }
}
