package com.example.demo.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")

@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id",
            nullable = false, updatable = false)
    private User user;
    @ManyToMany (fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "items_orders",
             joinColumns = { @JoinColumn(name = "order_id", referencedColumnName = "id",
                 nullable = false, updatable = false)},
        inverseJoinColumns = {
       @JoinColumn(name = "item", referencedColumnName = "name",
        nullable = false, updatable = false)})
    private Set<MenuItem> item;
    private double totalPrice;

    public Order(){};
    public Order(int id, int userid) {
        super();
        this.id = id;
        this.user=new User(userid,"","",true,"");
    }
    public Order(int id, User user, Set<MenuItem> items) {
        super();
        this.id = id;
        this.item=items;
        this.user=user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User  user) {
        this.user= user;
    }

    public Set<MenuItem> getItems() {
     return item;
    }

    public void setItems(Set<MenuItem> item) {
        this.item = item;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }
}
