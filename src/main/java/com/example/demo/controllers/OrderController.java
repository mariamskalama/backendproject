package com.example.demo.controllers;

import com.example.demo.interfaces.OrderInterface;
import com.example.demo.models.Order;
import com.example.demo.services.OrderService;
import com.example.demo.models.User;
import org.apache.tomcat.util.net.openssl.ciphers.Authentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@RestController
public class OrderController implements OrderInterface {
    @Autowired
    private OrderService orderService;


    public List<Order> getOrder( HttpServletRequest request) {
        return orderService.getAllOrders(request.getUserPrincipal().getName());
    }

    public void addOrder(@RequestBody Order order,HttpServletRequest request){
        Principal principal = request.getUserPrincipal();

        orderService.addOrder(order,request.getUserPrincipal().getName());
    }

    }
