package com.example.demo.controllers;

import com.example.demo.exceptions.DataNotFoundException;
import com.example.demo.interfaces.MenuItemInterface;
import com.example.demo.models.MenuItem;
import com.example.demo.models.Order;
import com.example.demo.services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
public class MenuItemController implements MenuItemInterface {

    @Autowired
    private MenuItemService menuItemService;


    public List<MenuItem> getAllMenuItemsForOrder(@PathVariable int orderId){

        List<MenuItem> returnItems= getAllMenuItemsForOrder(orderId);
        if(returnItems.size()==0)
            throw new DataNotFoundException("No items in menu");
        else
            return returnItems;
    }

    public List<MenuItem> getAllMenuItems(){
        List<MenuItem> returnItems= menuItemService.getAllMenuItems();
        if(returnItems.size()==0)
            throw new DataNotFoundException("No items in menu");
        else
            return returnItems;

    }

    public HttpStatus addMenuItem(@RequestBody MenuItem menuItem, HttpServletRequest request){
       return menuItemService.addMenuItem(menuItem,request.getUserPrincipal().getName());
    }


    public Order addMenuItemToCart(@RequestBody Set<MenuItem> menuItem , HttpServletRequest request){

      Order order= menuItemService.addMenuItemToCart(menuItem , request.getUserPrincipal().getName());
        if(order==null)
            throw new DataNotFoundException("No items in order");
      return order;
            }
}
