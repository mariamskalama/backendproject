package com.example.demo.controllers;

import com.example.demo.exceptions.DataNotFoundException;
import com.example.demo.interfaces.UserInterface;
import com.example.demo.services.UserService;
import com.example.demo.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
public class UserController implements UserInterface {
    @Autowired
    private UserService userService;

    public List<User> getAllUsers(){
       if(userService.getAllUsers().size()==0)
               throw new DataNotFoundException("No users");

           return userService.getAllUsers();
    }


    public Optional<User> getUser(@PathVariable int id) {
        if( userService.getUser(id)==null)
            throw new DataNotFoundException("No users with this id");
        return userService.getUser(id);
    }


    public UserDetails getUser(@PathVariable String name) {
        if( userService.loadUserByUsername(name)==null)
            throw new DataNotFoundException("No users with this name");

        return userService.loadUserByUsername(name);

    }

    //register
    public void addUser(@RequestBody User user){
        userService.addUser(user);


    }


    public void updateOrder(@RequestBody User user) {
        userService.updateUser(user);
    }


    public void deleteUser(@PathVariable int id) {
        userService.deleteUser(id);
    }
}
