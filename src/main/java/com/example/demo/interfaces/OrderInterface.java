package com.example.demo.interfaces;

import com.example.demo.models.Order;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;
@RestController
public interface OrderInterface {
    @RequestMapping("/orders")
    public List<Order> getOrder(HttpServletRequest request);

    @RequestMapping(method = RequestMethod.POST,value="/orders")
    public void addOrder(@RequestBody Order order, HttpServletRequest request);
}
