package com.example.demo.interfaces;

import com.example.demo.exceptions.DataNotFoundException;
import com.example.demo.models.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import java.util.Optional;
@RestController
public interface UserInterface {
    @RequestMapping("/users")
    public List<User> getAllUsers();


    @RequestMapping("/users/id/{id}")
    public Optional<User> getUser(@PathVariable int id);


    @RequestMapping("/users/name/{name}")
    public UserDetails getUser(@PathVariable String name);

    //register
    @RequestMapping(method = RequestMethod.POST,value="/users")
    public void addUser(@RequestBody User user);


    @RequestMapping(method = RequestMethod.PUT,value="/users")
    public void updateOrder(@RequestBody User user);


    @RequestMapping(method = RequestMethod.DELETE,value="/users/{userId}")
    public void deleteUser(@PathVariable int id);
}


