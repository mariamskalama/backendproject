package com.example.demo.interfaces;

import com.example.demo.models.MenuItem;
import com.example.demo.models.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;
@RestController
public interface MenuItemInterface {


    @RequestMapping("/orders/{orderId}/menuItem")
    public List<MenuItem> getAllMenuItemsForOrder(@PathVariable int orderId);

    @RequestMapping("/menuItem")
    public List<MenuItem> getAllMenuItems();

    @RequestMapping(method = RequestMethod.POST,value="/menuItem",consumes = MediaType.APPLICATION_JSON_VALUE)
    public HttpStatus addMenuItem(@RequestBody MenuItem menuItem, HttpServletRequest request);

    @RequestMapping(method = RequestMethod.POST,value="/menuItemtoCart")
    public Order addMenuItemToCart(@RequestBody Set<MenuItem> menuItem , HttpServletRequest request);
}
