package com.example.demo;

import com.example.demo.repositories.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
@EntityScan

@SpringBootApplication
@EnableAutoConfiguration

@EnableJpaRepositories(basePackageClasses = UserRepository.class)


public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
