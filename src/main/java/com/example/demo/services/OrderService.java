package com.example.demo.services;


import com.example.demo.exceptions.DataNotFoundException;
import com.example.demo.models.MenuItem;
import com.example.demo.models.Order;
import com.example.demo.models.User;
import com.example.demo.repositories.OrderRepository;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class  OrderService {
    private static final Logger logger = LogManager.getLogger(OrderService.class);
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private UserService userService;

public OrderService(){}
public OrderService(OrderRepository orderRepository, UserService userService){
    this.orderRepository=orderRepository;
    this.userService=userService;
}

    public List<Order> getAllOrders(int userId){
        BasicConfigurator.configure();
        logger.info("Getting order history for user by id" );


        List<Order> orders=new ArrayList<>();
        orderRepository.findByUserId(userId).forEach(orders::add);
        if(orders.size()==0){
            logger.info("throwing exception" );

            throw new DataNotFoundException("No previous orders for this user");}
        logger.info("returning order history" );

        return orders;
    }
    public List<Order> getAllOrders(String name){
        BasicConfigurator.configure();
        logger.info("getting order history for user by name" );

        List<Order> orders=new ArrayList<>();
        orderRepository.findByUserUserName(name).forEach(orders::add);

        System.out.println(name);
        if(orders.size()==0){
            logger.info("throwing exception" );

            throw new DataNotFoundException("No previous orders for this user");}
        logger.info("returning order history" );

        return orders;
    }
    public void addOrder(Order order, String name){
        logger.info("adding" );

        BasicConfigurator.configure();

        User orderUser=null;

        List<User> allUsers=userService.getAllUsers();
        for(User user : allUsers){
            if(user.getUserName().equals(name)){
                orderUser=user;
            }
        }
        order.setUser(orderUser);
        double price=0;
        if(order.getItems()!=null){
        for(MenuItem item : order.getItems()){
            price+= item.getPrice();
        }
        order.setTotalPrice(price); }


        orderRepository.save(order);
        logger.info("returning saved" );


    }

    public List<Order> getOrder(int id) {
        return orderRepository.findById(id);
    }

    public void updateOrder(Order order){
        orderRepository.save(order);
    }

    public void deleteOrder(Order order){
        orderRepository.delete(order);

    }
}
