package com.example.demo.services;

import com.example.demo.models.MyuserDetails;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class UserService implements UserDetailsService {
    private static final Logger logger = LogManager.getLogger(UserService.class);
 @Autowired
 private UserRepository userRepository;

    public UserService(){}

public UserService(UserRepository userRepository){
    this.userRepository=userRepository;
}
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
    Optional<User> user=userRepository.findByUserName(s);
    user.orElseThrow(()->new UsernameNotFoundException("Not found "+s));
    return user.map(MyuserDetails::new).get();
        }

    public List<User> getAllUsers(){
        BasicConfigurator.configure();
        logger.info("getting all users" );


        List<User> users=new ArrayList<>();
        userRepository.findAll().forEach(
                users::add);
        System.out.println("users" + users.size());

        return users;
    }

    public User addUser(User user){
        BasicConfigurator.configure();
        logger.info("registering new user" );
        userRepository.save(user);
        logger.info("registered" );
        return user;
    }

    public Optional<User> getUser(int id) {
        return userRepository.findById(id);
    }

    public void updateUser(User user){
        userRepository.save(user);
    }

    public void deleteUser(int id){
        userRepository.deleteById(id);
    }





}
