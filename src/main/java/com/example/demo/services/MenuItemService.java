package com.example.demo.services;

import com.example.demo.exceptions.DataNotFoundExceptionMapper;
import com.example.demo.models.MenuItem;
import com.example.demo.models.Order;
import com.example.demo.models.User;
import com.example.demo.repositories.MenuItemRepository;
import com.example.demo.repositories.OrderRepository;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.List;

@Service
public class MenuItemService {
    private static final Logger logger = LogManager.getLogger(MenuItemService.class);

    @Autowired
    private MenuItemRepository menuItemRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private OrderRepository orderRepository;


    public MenuItemService(){};
    public MenuItemService(MenuItemRepository menuItemRepository, OrderRepository orderRepository, UserService userService){
        this.menuItemRepository=menuItemRepository;
        this.orderRepository=orderRepository;
        this.userService=userService;
    };


    public List<MenuItem> getAllMenuItemsForOrder(int orderId){
        BasicConfigurator.configure();

        logger.info("getting menu items by order id" );
        List<MenuItem> menuItems=new ArrayList<>();

        menuItemRepository.findByOrderId(orderId).forEach(menuItems::add);
        logger.info("returning menu items" );

        return menuItems;
    }
    public List<MenuItem> getAllMenuItems(){
        BasicConfigurator.configure();

        logger.info("getting menu items " );

        List<MenuItem> menuItems=new ArrayList<>();
        menuItemRepository.findAll().forEach(menuItems::add);
        logger.info("returning menu items" );

        return menuItems;

    }
    public HttpStatus addMenuItem(MenuItem menuItem, String name){
        BasicConfigurator.configure();

        logger.info("adding menu items" );

        User orderUser=null;

        List<User> allUsers=userService.getAllUsers();
        for(User user : allUsers){
            if(user.getUserName().equals(name)){
                orderUser=user;
            }}
        if(orderUser.getRoles().equals("ADMIN")){
            logger.info("accepted" );

        //menuItemRepository.save(menuItem);
        return HttpStatus.ACCEPTED;
       }
        else{
            logger.info("rejected" );

        return HttpStatus.UNAUTHORIZED;}
    }

   public Order addMenuItemToCart(Set<MenuItem> menuItems, String name){
       BasicConfigurator.configure();

       logger.info("placing order" );

       User orderUser=null;
      double price=0;
        List<User> allUsers=userService.getAllUsers();
        for(User user : allUsers){
            if(user.getUserName().equals(name)){
                orderUser=user;
            }}

       Order order =new Order(0, orderUser,menuItems);
       Set<Order> orderSet=new HashSet<>();
       for(MenuItem item : menuItems){
           price+=item.getPrice();
           if(item.getOrder()!=null){
               orderSet=item.getOrder();}
               orderSet.add(order);
              item.setOrder(orderSet);
              menuItemRepository.save(item);
       }
       order.setTotalPrice(price);
       orderRepository.save(order);


       logger.info(order.getTotalPrice());
       logger.info(price);

       logger.info("order placed" );

       return order;

    }


    public void addMenuItemToOrder(MenuItem menuItem){
        Order neworder=new Order();
        menuItemRepository.save(menuItem);
    }

    public Optional<MenuItem> getOrder(String id) {
        return menuItemRepository.findById(id);
    }

    public void updateMenuItem(MenuItem menuItem){
        menuItemRepository.save(menuItem);
    }

    public void deleteOrder(String id){
        menuItemRepository.deleteById(id);
    }
}
